from rest_framework import mixins, generics

from agendamento.models import Agendamento
from agendamento.serializers import AgendamentoSerializer
import logging

logger = logging.getLogger(__name__)


class AgendamentoList(mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      generics.GenericAPIView):
    serializer_class = AgendamentoSerializer

    def get_queryset(self):
        logger.info('Pegando todos os agendamentos')
        queryset = Agendamento.objects.all()
        sala = self.request.query_params.get('sala', None)
        data_inicio = self.request.query_params.get('data_inicio', None)
        data_fim = self.request.query_params.get('data_fim', None)

        if sala:
            logger.info('Filtro por sala encontrado, SALA = %s' % sala)
            queryset = queryset.filter(sala_id=sala)
        if data_fim:
            logger.info('Filtro por data_fim encontrado, DATA_FIM = %s' % data_fim)
            queryset = queryset.filter(data_fim__date=data_fim)
        if data_inicio:
            logger.info('Filtro por data_inicio encontrado, DATA_INICIO = %s' % data_inicio)
            queryset = queryset.filter(data_inicio__date=data_inicio)

        return queryset

    def get(self, request, *args, **kwargs):
        logger.info('Pegando todos os agendamentos')
        try:
            return self.list(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))

    def post(self, request, *args, **kwargs):
        logger.info('Criando novo agendamento')
        try:
            return self.create(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))


class AgendamentoDetail(mixins.RetrieveModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin,
                        generics.GenericAPIView):
    queryset = Agendamento.objects.all()
    serializer_class = AgendamentoSerializer

    def get(self, request, *args, **kwargs):
        logger.info('Pegando agendamento ID = %s' % kwargs['pk'])
        try:
            return self.retrieve(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))

    def put(self, request, *args, **kwargs):
        logger.info('Atualizando agendamento ID = %s' % kwargs['pk'])
        try:
            return self.update(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))

    def delete(self, request, *args, **kwargs):
        logger.info('Deletando agendamento ID = %s' % kwargs['pk'])
        try:
            return self.destroy(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))
