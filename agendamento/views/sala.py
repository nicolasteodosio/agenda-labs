from rest_framework import mixins, generics

from agendamento.models import Sala
from agendamento.serializers import SalaSerializer
import logging

logger = logging.getLogger(__name__)


class SalaList(mixins.ListModelMixin,
               mixins.CreateModelMixin,
               generics.GenericAPIView):
    queryset = Sala.objects.all()
    serializer_class = SalaSerializer

    def post(self, request, *args, **kwargs):
        logger.info('Criando nova sala')
        try:
            return self.create(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))


class SalaDetail(mixins.RetrieveModelMixin,
                 mixins.UpdateModelMixin,
                 mixins.DestroyModelMixin,
                 generics.GenericAPIView):
    queryset = Sala.objects.all()
    serializer_class = SalaSerializer

    def get(self, request, *args, **kwargs):
        logger.info('Pegando sala ID = %s' % kwargs['pk'])
        try:
            return self.retrieve(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))

    def put(self, request, *args, **kwargs):
        logger.info('Atualizando sala ID = %s' % kwargs['pk'])
        try:
            return self.update(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))

    def delete(self, request, *args, **kwargs):
        logger.info('Deletando sala ID = %s' % kwargs['pk'])
        try:
            return self.destroy(request, *args, **kwargs)
        except Exception as e:
            logger.error(str(e))
