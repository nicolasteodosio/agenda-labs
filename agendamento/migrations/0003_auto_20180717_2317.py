# Generated by Django 2.0.7 on 2018-07-17 23:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agendamento', '0002_agendamento'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agendamento',
            name='data_fim',
            field=models.DateTimeField(verbose_name='Data fim'),
        ),
    ]
