from django.db.models import Q
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from agendamento.models import Sala, Agendamento

import logging

logger = logging.getLogger(__name__)


class SalaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sala
        fields = '__all__'


class AgendamentoSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        if attrs['data_inicio'] > attrs['data_fim']:
            logger.warning('Data de inicio maior que data fim')
            raise serializers.ValidationError('Data de fim tem que ser maior que a de inicio')

        agendamentos = Agendamento.objects.filter(sala=attrs['sala'])
        if agendamentos:
            agendamentos_data = agendamentos.filter(
                Q(data_inicio__range=(attrs['data_inicio'], attrs['data_fim']))
                | Q(data_fim__range=(attrs['data_inicio'], attrs['data_fim']))

            )
            if agendamentos_data:
                logger.warning('Já existe um agendamento para as datas escolhidas')
                raise serializers.ValidationError('Já existe um agendamento nessa sala com essa data escolhida, '
                                                  'Agendamento ID = {}'
                                                  .format(list(agendamentos_data.values_list('id', flat=True))))
        return attrs

    class Meta:
        model = Agendamento
        fields = '__all__'
        validators = [
            UniqueTogetherValidator(
                queryset=Agendamento.objects.all(),
                fields=('sala', 'data_inicio', 'data_fim'),
                message='Já existe um agendamento pra essa sala nas datas escolhidas.'
            )
        ]
