from django.db import models


class Sala(models.Model):
    nome = models.CharField('Nome', max_length=100)

    def __str__(self):
        return self.nome


class Agendamento(models.Model):
    titulo = models.CharField('Titulo', max_length=200)
    sala = models.ForeignKey(Sala, on_delete=models.DO_NOTHING)
    data_inicio = models.DateTimeField('Data de inicio')
    data_fim = models.DateTimeField('Data fim')

    def __str__(self):
        return self.titulo