import json

from django.shortcuts import resolve_url
from django.test import TestCase
from model_mommy import mommy

from agendamento.models import Sala


class SalaDetailTest(TestCase):

    def test_get_sala(self):
        mommy.make(Sala, id=123, nome='teste')

        response = self.client.get(resolve_url('salas_detail', 123))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"id":123,"nome":"teste"}')

    def test_get_sala_retorna_404_quando_nao_existe(self):
        response = self.client.get(resolve_url('salas_detail', 90))

        self.assertEqual(response.status_code, 404)

    def test_delete_sala(self):
        salas = mommy.make(Sala, _quantity=5)

        response = self.client.delete(resolve_url('salas_detail', salas[0].id))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Sala.objects.count(), 4)

    def test_put_sala(self):
        mommy.make(Sala, id=99, nome='teste')
        payload = {
            'nome': 'mudei'
        }
        response = self.client.put(resolve_url('salas_detail', 99),  json.dumps(payload),
                                   content_type="application/json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"id":99,"nome":"mudei"}')

    def test_put_sala_falha_titulo_obrigatorio(self):
        mommy.make(Sala, id=99, nome='teste')
        payload = {
        }
        response = self.client.put(resolve_url('salas_detail', 99),  json.dumps(payload),
                                   content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"nome":["This field is required."]}')

    def test_post_sala(self):
        payload = {
            'nome': 'sela testee'
        }
        response = self.client.post(resolve_url('salas_list'), json.dumps(payload),
                                    content_type="application/json")

        self.assertEqual(response.status_code, 201)
        self.assertTrue(Sala.objects.filter(nome='sela testee').exists())