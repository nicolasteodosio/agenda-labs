import json

from datetime import datetime, timedelta
from django.shortcuts import resolve_url
from django.test import TestCase
from model_mommy import mommy

from agendamento.models import Sala, Agendamento


class AgendamentoListTest(TestCase):

    def test_list_agendamento(self):
        mommy.make(Agendamento, _quantity=7)

        response = self.client.get(resolve_url('agendamento_list'))

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.content)

    def test_list_agendamento_mesmo_vazio(self):
        response = self.client.get(resolve_url('agendamento_list'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[]')

    def test_list_agendamento_filtrando_sala(self):
        sala = mommy.make(Sala, id=1)
        sala5 = mommy.make(Sala, id=5)
        mommy.make(Agendamento, sala=sala)
        mommy.make(Agendamento, sala=sala5)

        response = self.client.get(resolve_url('agendamento_list'), {'sala': 1})

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.content)

    def test_list_agendamento_filtrando_sala_vazio(self):
        sala = mommy.make(Sala, id=1)
        sala5 = mommy.make(Sala, id=5)
        mommy.make(Agendamento, sala=sala)
        mommy.make(Agendamento, sala=sala5)

        response = self.client.get(resolve_url('agendamento_list'), {'sala': 99})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[]')

    def test_list_agendamento_filtrando_data_inicio(self):
        data = datetime.now() - timedelta(days=5)
        mommy.make(Agendamento, data_inicio=data)
        mommy.make(Agendamento, data_inicio=datetime.now())

        response = self.client.get(resolve_url('agendamento_list'), {'data_inicio': data.strftime('%Y-%m-%d')})

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.content)

    def test_list_agendamento_filtrando_data_fim(self):
        data = datetime.now() - timedelta(days=5)
        mommy.make(Agendamento, data_fim=data)
        mommy.make(Agendamento, data_fim=datetime.now())

        response = self.client.get(resolve_url('agendamento_list'), {'data_fim': data.strftime('%Y-%m-%d')})

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.content)

    def test_list_agendamento_filtrando_data_fim_vazio(self):
        data = datetime.now() - timedelta(days=5)
        mommy.make(Agendamento, data_fim=datetime.now())

        response = self.client.get(resolve_url('agendamento_list'), {'data_fim': data.strftime('%Y-%m-%d')})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[]')

    def test_list_agendamento_filtrando_data_inicio_vazio(self):
        data = datetime.now() - timedelta(days=5)
        mommy.make(Agendamento, data_inicio=datetime.now())

        response = self.client.get(resolve_url('agendamento_list'), {'data_inicio': data.strftime('%Y-%m-%d')})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'[]')

    def test_list_agendamento_post(self):
        sala = mommy.make(Sala)
        payload = {
            'titulo': 'testee',
            'data_inicio': str(datetime.now()),
            'data_fim': str(datetime.now()),
            'sala': sala.id
        }

        response = self.client.post(resolve_url('agendamento_list'),
                                    json.dumps(payload), content_type="application/json")

        self.assertEqual(response.status_code, 201)
        self.assertEqual(Agendamento.objects.all().count(), 1)

    def test_list_agendamento_post_data_inicio_maior_que_fim(self):
        sala = mommy.make(Sala)
        payload = {
            'titulo': 'testee',
            'data_inicio': str(datetime.now() + timedelta(days=8)),
            'data_fim': str(datetime.now()),
            'sala': sala.id
        }

        response = self.client.post(resolve_url('agendamento_list'),
                                    json.dumps(payload), content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content,  b'{"non_field_errors":["Data de fim tem que ser maior que a de inicio"]}')

    def test_list_agendamento_post_ja_existe_agendamento(self):
        sala = mommy.make(Sala)
        mommy.make(Agendamento, sala=sala, id=123, data_inicio=datetime.now(),
                   data_fim=datetime.now() + timedelta(days=4))
        payload = {
            'titulo': 'testee',
            'data_inicio': str(datetime.now() + timedelta(days=2)),
            'data_fim': str(datetime.now() + timedelta(days=5)),
            'sala': sala.id
        }

        response = self.client.post(resolve_url('agendamento_list'),
                                    json.dumps(payload), content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content,  b'{"non_field_errors":["J\xc3\xa1 existe um agendamento nessa sala com essa '
                                            b'data escolhida, Agendamento ID = [123]"]}')


class AgendamentoDetailTest(TestCase):
    def test_delete_agendamento(self):
        agendamentos = mommy.make(Agendamento, _quantity=5)

        response = self.client.delete(resolve_url('agendamento_detail', agendamentos[0].id))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Agendamento.objects.count(), 4)

    def test_put_agendamento(self):
        sala = mommy.make(Sala)
        mommy.make(Agendamento, id=99, titulo='teste', sala=sala)
        payload = {
            'titulo': 'mudei',
            'data_inicio': str(datetime.now()),
            'data_fim': str(datetime.now()),
            'sala': sala.id
        }
        response = self.client.put(resolve_url('agendamento_detail', 99),  json.dumps(payload),
                                   content_type="application/json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Agendamento.objects.get(id=99).titulo, 'mudei')

    def test_put_agendamento_falha_sala_obrigatorio(self):
        sala = mommy.make(Sala)
        mommy.make(Agendamento, id=99, titulo='teste', sala=sala)
        payload = {
            'titulo': 'mudei',
            'data_inicio': str(datetime.now()),
            'data_fim': str(datetime.now()),
        }
        response = self.client.put(resolve_url('agendamento_detail', 99),  json.dumps(payload),
                                   content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"sala":["This field is required."]}')

    def test_put_agendamento_falha_titulo_obrigatorio(self):
        sala = mommy.make(Sala)
        mommy.make(Agendamento, id=99, titulo='teste', sala=sala)
        payload = {
            'data_inicio': str(datetime.now()),
            'data_fim': str(datetime.now()),
            'sala': sala.id
        }
        response = self.client.put(resolve_url('agendamento_detail', 99),  json.dumps(payload),
                                   content_type="application/json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"titulo":["This field is required."]}')

    def test_put_agendamento_falha_data_inicio_obrigatorio(self):
        sala = mommy.make(Sala)
        mommy.make(Agendamento, id=99, titulo='teste', sala=sala)
        payload = {
            'titulo': 'mudei',
            'data_fim': str(datetime.now()),
            'sala': sala.id
        }
        response = self.client.put(resolve_url('agendamento_detail', 99),  json.dumps(payload),
                                   content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"data_inicio":["This field is required."]}')

    def test_put_agendamento_falha_data_fim_obrigatorio(self):
        sala = mommy.make(Sala)
        mommy.make(Agendamento, id=99, titulo='teste', sala=sala)
        payload = {
            'titulo': 'mudei',
            'data_inicio': str(datetime.now()),
            'sala': sala.id
        }
        response = self.client.put(resolve_url('agendamento_detail', 99),  json.dumps(payload),
                                   content_type="application/json")

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.content, b'{"data_fim":["This field is required."]}')
