### Instalação ###
* Clone o projeto `git clone git@bitbucket.org:nicolasteodosio/agenda-labs.git`
* Projeto foi criando unsando python 3, então cria um virtualenv com python 3
* Recomendo o uso do  [pyenv](https://github.com/pyenv/pyenv-installer) e [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv#installation)
* Ative seu virtualenv
* [Instale o docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository)
* [Instale o docker-compose](https://docs.docker.com/compose/install/#install-compose)
* Na raiz do projeto execute `pip install -r requirements-dev.txt`
* Rode também sudo docker-compose up, para iniciar o banco de dados

### Rodando a aplicação ###
* Depois de tudo instalado e rodando o docker-compose execute `python manage.py migrate`, assim terá as tabelas da aplicação
* Finalmente execute o `python manage.py runserver`

### Rodando os testes  e coverage ###
* docker-compose tem que estar rodando
* Para executar os testes `python manage.py test --settings=app.settings_test --keepdb`
* Para saber o coverage `coverage run --source='.' manage.py test agendamento  --settings=app.settings_test --keepdb`
* `coverage report`

### Usando api ###
* O projeto foi feito usando django e django rest framework
* Com a aplicação rodando provavelmente na porta 8000
* http://localhost:8000/salas/[pk]
* http://localhost:8000/agendamento/
* http://localhost:8000/agendamento/?sala=[sala_id]
* http://localhost:8000/agendamento/?data_inicio=[YYYY-MM-DD]
* http://localhost:8000/agendamento/?data_fim=[YYYY-MM-DD]
* http://localhost:8000/agendamento/[pk]